**IT Governance and Security Framework for WFH**

1. **Background:** 

The  aim  of  this  policy  is  to  establish  guidelines  and  criteria  for  connecting  to  Clusus’s infrastructure and services while working from home. These guidelines are crafted to uphold a  robust  level  of  security  and  mitigate  the  potential  risks  associated  with  internal  and external threats to Clusus’s services. 

Clusus  employees  engaging  in  connections  to  services  hosted  on  Clusus’s  cloud infrastructure or internal network from remote locations are accountable for adhering to the specifications outlined in this policy. 

2. **Current Scenario of WFH:** 

The traditional office-based model has undergone significant transformation, with remote work becoming a prevalent and viable option for many professionals. However, it includes both merits and demerits. 

1. **Pros:** 
- **Business Continuity:** Supporting  work from  any  place  to  any  client  at  any  time (during epidemics or natural disasters) to keep the business operation running. 
- **Access to Global Talents:** Employers can get access to global talent and are not limited to hiring employees from a single location. 
- **Better Monitoring and Documentation:** Enhanced documentation of support tasks and project tasks on tools like Gitlab, as well as better monitoring of employees' performance regarding attendance and time management. 
- **Infrastructure Accessibility:** Work from home does not require physical presence to access cloud servers or on-premises physical servers (can be accessed through a secure VPN channel). 
- **Flexibility:** Working from home allows for a more flexible schedule, which can be especially beneficial for individuals who work outside office hours. Employees can work from any location they want in their personalized comfortable workspace. 
- **Time and Cost Savings:** Employees can save time and money on commuting costs, such as petrol and public transportation, leading to environmental sustainability by reducing  carbon  emissions.  Employers  can  also  save  on  office  space  and  related expenses. 
- **Access to a Broader Talent Pool:** Employers can access a larger and more diverse talent pool by hiring remote workers, as geographical location is no longer a limiting factor for connecting with global talent. 
- **Continuity of Operations:** With the infrastructure in place for employees to work remotely, businesses can continue their operations even when faced with external challenges  such  as  natural  disasters,  pandemics,  or  other  emergencies  that  may prevent access to the physical workplace. 
- **Work-life Balance:** Working from home can help individuals achieve a better work- life balance by reducing the time and stress associated with commuting, allowing for more  time  with family  and  the pursuit of  personal  interests,  better  physical  and mental health. 
- **Increased  Productivity:**  Employees  find  that  they  are  more  productive  when working  from  home,  as  they  can  create  a  comfortable  and  personalized  work environment that minimizes distractions. 
2. **Cons:** 
- **Work-life  Balance:**  The  risk  of  overworking  is  high  during  WFH,  which  directly impacts the work-life balance of Support Engineers. 
- **Lack of Socialization:** WFH limits face-to-face social interaction between colleagues and team members, resulting in a lack of bonding. 
- **Difficulty  in  Collaboration:**  Differences  in  time  zones  create  obstructions  in teamwork. Due to the lack of physical presence of all departments in the office, there is an impact on smooth collaboration between these departments. 
- **Lack of Physical Security:** Office devices are less secure at home, and hardware devices are more likely to break due to mishandling by other family members of employees. 
- **Data Security:** Home networks could be less secure than office networks, making data more vulnerable to outside intruders and hackers. 
- **Internet/Electricity Reliability:** Offices may have backup internet and electricity, but at home, internet or power cuts could result in decreased productivity. 
- **Software Licensing:** Remote work requires additional licensed software like Zoom, Teams, and Slack, which could add more financial burden to employers. 
- **Limited Features with Unlicensed Software:** Using unlicensed software may limit certain features. 
- **Delay in Communication:** Employees working in different time zones could result in communication delays and scheduling conflicts, impacting team efforts. 
- **Limited IT Support:** Limited IT support for employees during hardware/software failures. 
- **Lack of Proper Monitoring:** Simple errors like forgetting to start Hubstaff or time tracking on GitLab could misguide management regarding employees' actual worked hours/efforts. Employees could bypass time tracking by adding more worked hours in GitLab than the actual worked hours. 
3. **IT Governance and Security Framework for WFH Practice:** 

In response to the identified pros and cons, the following policies/procedures have been formulated for TeamNepal support Engineers who are working remotely. 

1. **Physical Security of Assets:** 
- Provision of suitable equipment, lockable cabinets, and storage furniture for remote working activities. Privately-owned equipment not under the organization's control is not allowed. 
- Proper disposal of printed information should be done. 
2. **Assigning Asset Owner:** 
- Employees should be declared as the asset owner during asset handover and should be held accountable in case of any future damage to the assets. Any prior damage to the asset before handover should be identified by the Office Administration. 
3. **Maintenance of Assets:** 
- Employees  should  perform  annual  servicing/maintenance  of  their  devices  at  the official service center allocated by the company. 
4. **Secured Network:** 
- Public Wi-Fi (airports, hotels, restaurants, etc.) should not be used for official tasks. 
- Licensed antivirus software or community versions listed by the office should be installed and scanned regularly. 
5. **Data Protection Policy:** 
- Data Loss Insurance and Cyber insurance can be enforced by the company. 
- Regular data backup should be maintained. 
- Installation of device location tracking, remote wipe capabilities, and execution in case of data breach. 
- Classification  of  information,  internal  systems,  and  services  to  determine  which remote employees are allowed or disallowed to store them locally. 
6. **Power and Network Backup:** 
- Office should provide or facilitate router backups and UPS. Laptop batteries should be in top condition, providing utmost battery backup. 
- Use power-saving modes during non-essential tasks to conserve battery power. 
7. **Streamlining Collaboration Tools:** 
- Discourage  the  use  of  multiple  software.  Encourage  the  use  of  a  single  licensed software like MS Teams that fulfills all requirements. 
8. **Time Zone Coordination:** 
- Use tools that display multiple time zones or provide a clear indication of the local time for each team member. 
- Follow calendars strictly for official communications. 
 
9. **Task Segmentation and Goal-Oriented Evaluation:** 
- Segregate tasks into smaller, manageable goals. 
- Define each goal, ensuring they are specific, measurable, achievable, relevant, and time-bound (SMART). 
- Assign a target date for each goal. 
- Monitor and track the progress of each goal for employee evaluation. 
4. **IT Governance and Security Framework Procedure for WFH:** 
1. **Start of Day Procedure:** 
- Start HubStaff and inform team through WhatsApp channel for Check-in. 
- System  check:  Support  Engineers  should  check  the  health  of  their  machines  by running a system health check script (refer to the appendix) 
- Planning of the day: Support Engineers should make a comprehensive plan to carry out the activities for the day.  
- Review all stories and tickets: Support Engineer should check all the stories, tickets from previous days and update where needed. 
- Attend team meetings: Support Engineer should attend their daily stand-up meetings and present his plan for the day with a last day update. Also, the support engineer should attend all the planned meetings for the day. 
2. **During the Day:** 
- When a customer creates a ticket, support engineer needs to acknowledge the ticket as soon as possible (within 10 minutes) 
- Based on the urgency of ticket(s), the support engineer needs to gather detailed information from customers regarding the problem. It could include Open ended questions, closed ended questions, logs etc. If required, the issue can be replicated in the local environment as well. 
- Support  engineer  needs  to  work  with  high  diligence  to  resolve  the  customer's problem. 
- Support engineers should try every possible way to resolve the ticket by collaborating within the support team and should escalate out to development team or high-level support only when none of the attempt to resolve ticket succeed. 
3. **Communication:** 
- Support Engineer needs to maintain regular and transparent communication with the customer throughout the resolution process. 
- Clearly articulate steps being taken to address the issue and provide updates on progress. 
- If  the  issue  requires  input  from  within  the  team  or  other  teams  or  team  lead, collaborate with them to ensure a comprehensive and accurate resolution. 
- Share  relevant  information  with  colleagues  to  facilitate  knowledge  sharing  and learning within the support team. 
4. **Team Collaboration and Active Participation** 
- Support  engineer  needs  to  actively  participate  and  collaborate  with  the  team  to resolve the issue reported by customer. 
3. **End of Day Procedure:** 
- Keep detailed records of interactions with customers. 
- Update tickets and review task summaries. 
- Include all the steps taken and solutions provided at the RCA section of the support ticket. 
- Seek feedback from customers. 
- Use feedback for improvement. 
4. **Check-out** 
- During check-out, remember to update all the Gitlab stories and customer support ticket. Don't miss out mentioning the updates in Helpdesk and Gitlab you carried throughout the day. 
- Inform other team members through Whatsapp Attendance group for the check-out. 
5. **Emergency Procedures:** 
1. **Connectivity Issue:** 
- In case of any network connection issue, Support engineers should inform the team immediately  so  that  a  backup  resource  can  be  deployed  to  continue  the  work. Moreover, in case of urgent issues, engineers should manage to connect using mobile data. 
2. **Equpment Failure:** 
- In the event of equipment failure during working hours, support engineers should promptly notify the office administration. They will provide guidance on whether to visit  the  office  for  equipment  replacement  or  the  designated  service  center  for repairs. 
3. **Urgent Production blocking issue:** 
- In  case  of  urgent  production  issues,  support  engineers  are  required  to  initiate investigations promptly. Immediate tasks, such as collecting logs and checking site connections, should be undertaken without delay. 
- If the support engineer is unable to resolve the issue, they should promptly seek assistance from their team lead. The team lead will then coordinate the involvement of senior support engineers or developers, if necessary, to facilitate a resolution. 
4. **Escalation Procedures:** 
- Support engineers are required to proactively follow up with clients every two days regarding existing issues, ensuring consistent communication and keeping clients informed about the progress of issue resolution. 
- Similarly, support engineers should maintain regular follow-ups with developers for issues under validation.  
- In the event of no response from clients or developers after three consecutive follow- ups, support engineers are to escalate the matter in consultation with their team lead and support manager. 4.6 War room and call meeting. 
6. **General Best Practices:** 
1. **Time Management:** 
- Establish a daily schedule with defined work hours and breaks. 
- Stay updated on technological trends. 
- Include short breaks in work hours for physical and mental well-being. 
2. **Professional Development** 
- Support engineers are required to proactively stay updated on current technological trends  within  their  domain.  This  includes  regularly  engaging  in  professional development activities, attending relevant training sessions, and staying informed about industry advancements. 
3. **Well-Being** 
- Support engineers are encouraged to include short breaks in their work hours. These breaks  can  involve  brief  physical  activities,  contributing  to  overall  fitness  and mitigating the risk of body strain and mental stress. 
7. **Physical Security of Assets:** 
- Employees should store sensitive materials and assets in lockable cabinets or storage furniture  
- Any printed information containing sensitive data should be disposed of securely. 
- Any personal storage devices like Hard drive, USB drives should not be used to store official information. 
8. **Assigning Asset Owner:** 
- Employees will sign an accountability agreement for each handed-over asset. 
- Employees  should  understand  that  they  will  be  held  responsible  for  any  future damage to the assigned assets. 
9. **Maintenance of Assets:** 
- Employees should maintain a comprehensive inventory of IT assets, categorizing by criticality. 
- Employees should develop routine maintenance schedules based on best practices. 
- Employees should utilize monitoring tools for early issue identification. 
- Employees  should  maintain  detailed  asset  documentation  and  generate  regular reports for performance analysis.  
- Employees should provide training on proper asset use and care. 
10. **Secured Network:** 
- Employees should use secure Wi-Fi connections, have a strong and unique Wi-Fi password and use the latest encryption technologies available such as WPA3. 
- Employees should regularly update their devices like routers and laptops. 
- If possible, employees should set up a separate guest network for your home devices and restrict access to your work devices.  
- Employees should periodically review the devices connected to your network and remove any unfamiliar or unauthorized devices. 
- Employees should avoid using public Wi-Fi connection when working in mainly client environments like UAT servers, production servers to avoid possible intrusion. 
11. **Security Scan:** 
- Employee must install latest Reputable licensed antivirus software and antimalware recommended by security team in their system. 
- Employees should enable real-time scanning and schedule a full scan of their device once every 7 days to avoid viruses or malware.  
- Employees  should  enable  automatic  updates  to  their  antivirus  and  antimalware software regularly. 
- Employees should take advantage of additional security features provided by your antivirus software, such as firewalls, email scanning, and web protection. 
12. **Data Protection:** 
- Employees should classify the information as critical and non-critical, for example payment information, username / password, Transaction one time passcode could be critical  and  transaction  date/time,  bank  names  could  be  non-critical.  Further employees also should be aware of the data security and its severity for both critical and non-critical information. 
- Employees should be aware of the phishing email and check Sender's email address, spelling and grammar, generic greetings, spam links, sense of urgency in email which could be the hint of spam emails. Employees should never respond, click on any links, download suspicious attachments from those kind of spam emails. 
13. **Power and Network Backup:** 
- Employees  should  Identify  critical  network  equipment,  including  routers  and switches  and  Implement  an  Uninterruptible  Power  Supply  (UPS)  for  essential network infrastructure. 
- Employees should follow guidelines for proper charging and discharging practices to extend   battery life (Be aware of the proper way to use charger and disconnect it when battery is full to maintain battery health). 
- Employees should use power-saving modes during non-essential tasks to conserve battery power (The office laptops should not be used for non-essential tasks, so the battery health doesn’t degrade unnecessarily). 
14. **Streamlining Collaboration Tools:** 
- Employees should assess existing collaboration tools and identify key functionalities. 
- Employees should select a single collaboration tool meeting identified requirements. 
- Employees should conduct a pilot with user feedback on usability and performance. 
- Employee should develop a comprehensive training program for the new tool. 
- Employee should Integrate the new tool with existing systems and ensure seamless data migration. 
- Employees should communicate reasons for the transition and highlight benefits. 
- Employee should collect post-implementation feedback plus Regularly assess tool performance and iterate based on user feedback and organizational needs. 
15. **Time Zone Coordination:** 
- Employees should clearly state their working status such as In, out, lunch break, no electricity,  no  internet  etc.  on  the  team's  official  communication  channel (WhatsApp/slack). 
- When scheduling meetings or setting deadlines, employees should be considerate of different time zones. Use tools that display multiple time zones or provide a clear indication of the local time for each team member. 
16. **Task Segmentation and Goal-Oriented Evaluation:** 
- Employees should segregate the identified task into smaller, manageable goals or milestones. 
- Employees  and  their  corresponding  team  leads  should  clearly  define  each  goal, ensuring that they are specific, measurable, achievable, relevant, and time-bound (SMART). 
- A target date for the completion of each goal should be assigned, ensuring that the overall timeline aligns with the project or task deadline. 
- Monitoring and tracking each goal's progress, on which each employee should be evaluated. 

Appendix A: Scripts 

A.1 Daily health check scripts: 

*#!/bin/bashs*** 

- *Function to check CPU usage*** 

*check\_cpu() {*** 

`  `*echo "Checking CPU Usage..."*** 

`  `*cpu\_usage=$(top -bn1 | grep "Cpu(s)" | awk '{print $2}' | awk -F. '{print $1}')   echo "CPU Usage: ${cpu\_usage}%"*** 

*}*** 

- *Function to check memory usage*** 

*check\_memory() {*** 

`  `*echo "Checking Memory Usage..."*** 

`  `*free\_memory=$(free -m | awk '/Mem:/ {print $3}')*** 

`  `*total\_memory=$(free -m | awk '/Mem:/ {print $2}')*** 

`  `*echo "Used Memory: ${free\_memory}MB out of ${total\_memory}MB"*** 

*}*** 

- *Function to check available disk space*** 

*check\_disk() {*** 

`  `*echo "Checking Disk Space..."*** 

`  `*disk\_space=$(df -h / | awk 'NR==2 {print $5}')*** 

`  `*echo "Disk Space Usage: ${disk\_space}"*** 

*}*** 

- *Function to check network connectivity*** 

*check\_network() {*** 

`  `*echo "Checking Network Connectivity..."*** 

`  `*ping -c 4 google.com > /dev/null 2>&1*** 

`  `*if [ $? -eq 0 ]; then*** 

`    `*echo "Network is reachable."*** 

`  `*else*** 

`    `*echo "Network is unreachable."*** 

`  `*fi*** 

*}*** 

- *Main function to call other health check functions main() {*** 

  `  `*check\_cpu*** 

`  `*check\_memory*** 

`  `*check\_disk*** 

`  `*check\_network*** 

*}*** 

- *Run the main function*** 

*main*** 
